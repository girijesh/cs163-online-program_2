// Girijesh Thodupunuri
// CS 163
// Program 2
// stack.cpp
// This file contains the implementation of the Stack class.
// The Stack class is used to manage toppings in the Pizza Management System.
// It uses a linear linked list of arrays to store and retrieve Topping objects.

#include "stack.h"
#include <cstring>
#include <iostream>

using namespace std;

// Constructor: Initializes the head pointer to nullptr
Stack::Stack() {
    head = nullptr;
}

// Destructor: Deallocates the memory used by the stack
Stack::~Stack() {
    Node* current = head;
    while (current != nullptr) {
        Node* temp = current;
        current = current->next;
        delete temp;
    }
}

// Push: Adds a new Topping object to the top of the stack
void Stack::push(const Topping& topping) {
    Node* newNode = new Node;
    newNode->topIndex = 0;
    newNode->toppings[0] = topping;
    newNode->next = head;
    head = newNode;
}

// Pop: Removes and returns the Topping object from the top of the stack
void Stack::pop(Topping& topping) {
    if (head == nullptr) {
        cout << "Stack underflow! Cannot pop from an empty stack." << endl;
        return;
    }

    topping = head->toppings[head->topIndex];
    head->topIndex--;

    if (head->topIndex < 0) {
        Node* temp = head;
        head = head->next;
        delete temp;
    }
}

// Peek: Returns the Topping object at the top of the stack without removing it
void Stack::peek(Topping& topping) {
    if (head == nullptr) {
        cout << "Stack is empty! Cannot peek." << endl;
        return;
    }

    topping = head->toppings[head->topIndex];
}

// Display: Prints the details of all Topping objects in the stack
void Stack::display() {
    if (head == nullptr) {
        cout << "Stack is empty!" << endl;
        return;
    }

    cout << "Toppings on the pizza:" << endl;
    Node* current = head;
    while (current != nullptr) {
        for (int i = current->topIndex; i >= 0; i--) {
            cout << "Topping: " << current->toppings[i].name << endl;
            cout << "Amount: " << current->toppings[i].amount << endl;
            cout << "Calories: " << current->toppings[i].calories << endl;
            cout << "Color: " << current->toppings[i].colorOfTopping << endl;
            cout << "------------------------" << endl;
        }
        current = current->next;
    }
}