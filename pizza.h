// Girijesh Thodupunuri
// CS 163
// Program 2
// pizza.h
// This file contains the struct definitions for Topping and Pizza.
// These structs are used throughout the program to represent and manage toppings and pizzas.

#ifndef PIZZA_H
#define PIZZA_H

// Struct to store information about a topping
struct Topping {
    char* name;        // Name of the topping
    int amount;        // Amount of the topping (e.g., weight or quantity)
    int calories;      // Calorie count of the topping
    char* colorOfTopping; // Color of the topping
};

// Struct to store information about a pizza
struct Pizza {
    char* name;        // Name of the pizza
    char* sauce;       // Type of sauce used
    int numToppings;   // Number of toppings on the pizza
    char* size;        // Size of the pizza
};

#endif // PIZZA_H
