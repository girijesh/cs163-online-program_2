// Girijesh Thodupunuri
// CS 163
// Program 2
// queue.h
// This file contains the declaration of the Queue class.
// The Queue class is used to manage pizzas in the Pizza Management System.
// It uses a circular linked list to store and retrieve Pizza objects.

#include "pizza.h"

class Queue {
public:
    // Constructor: Initializes the queue
    Queue();

    // Destructor: Deallocates the memory used by the queue
    ~Queue();

    // Enqueue: Adds a new Pizza object to the rear of the queue
    void enqueue(Pizza pizza);

    // Dequeue: Removes and returns the Pizza object from the front of the queue
    Pizza dequeue();

    // Peek: Returns the Pizza object at the front of the queue without removing it
    Pizza peek();

    // Display: Prints the details of all Pizza objects in the queue
    void display();

private:
    // Node struct: Represents a node in the circular linked list
    struct Node {
        Pizza pizza; // The Pizza object stored in the node
        Node* next;  // Pointer to the next node in the list
    };

    Node* front; // Pointer to the front node of the queue
    Node* rear;  // Pointer to the rear node of the queue
};