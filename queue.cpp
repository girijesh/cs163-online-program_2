// Girijesh Thodupunuri
// CS 163
// Program 2
// This file contains the implementation of the Queue class.
// The Queue class is used to manage pizzas in the Pizza Management System.
// It uses a circular linked list to store and retrieve Pizza objects.

#include "queue.h"
#include <cstring>
#include <iostream>

using namespace std;

// Constructor: Initializes the front and rear pointers to nullptr
Queue::Queue() {
    front = nullptr;
    rear = nullptr;
}

// Destructor: Deallocates the memory used by the queue
Queue::~Queue() {
    Node* current = front;
    while (current != nullptr) {
        Node* temp = current;
        current = current->next;
        delete temp;
    }
}

// Enqueue: Adds a new Pizza object to the rear of the queue
void Queue::enqueue(Pizza pizza) {
    Node* newNode = new Node;
    newNode->pizza = pizza;
    newNode->next = nullptr;

    if (front == nullptr) {
        front = newNode;
        rear = newNode;
    } else {
        rear->next = newNode;
        rear = newNode;
        rear->next = front; 
    }
}

// Dequeue: Removes and returns the Pizza object from the front of the queue
Pizza Queue::dequeue() {
    if (front == nullptr) {
        cout << "Queue is empty!" << endl;
        return Pizza();
    }

    Node* temp = front;
    Pizza pizza = temp->pizza;

    if (front == rear) {
        front = nullptr;
        rear = nullptr;
    } else {
        front = front->next;
        rear->next = front; 
    }

    delete temp;
    return pizza;
}

// Peek: Returns the Pizza object at the front of the queue without removing it
Pizza Queue::peek() {
    if (front == nullptr) {
        cout << "Queue is empty" << endl;
        return Pizza();
    }
    return front->pizza;
}

// Display: Prints the details of all Pizza objects in the queue
void Queue::display() {
    if (front == nullptr) {
        cout << "Queue is empty!" << endl;
        return;
    }

    cout << "Pizzas in the Queue:" << endl;
    Node* current = front;
    do {
        cout << "Pizza Name: " << current->pizza.name << endl;
        cout << "Sauce: " << current->pizza.sauce << endl;
        cout << "Number of Toppings: " << current->pizza.numToppings << endl;
        cout << "Size: " << current->pizza.size << endl;
        cout << "------------------------" << endl;
        current = current->next;
    } while (current != front);
}