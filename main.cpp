// Girijesh Thodupunuri
// CS 163
// Program 2
// This file contains the main function and the user interface for the Pizza Management System.
// It interacts with the user, displays a menu, and calls the appropriate functions based on user input.
// The program uses a Stack to manage toppings and a Queue to manage pizzas.

#include <iostream>
#include <cstring>
#include "stack.h"
#include "queue.h"

using namespace std;

// Function to display the menu options
void displayMenu() {
    cout << "Pizza Management System" << endl;
    cout << "1. Push Topping to Stack" << endl;
    cout << "2. Pop Topping from Stack" << endl;
    cout << "3. Peek Top Topping in Stack" << endl;
    cout << "4. Display Toppings in Stack" << endl;
    cout << "5. Enqueue Pizza to Queue" << endl;
    cout << "6. Dequeue Pizza from Queue" << endl;
    cout << "7. Peek Next Pizza in Queue" << endl;
    cout << "8. Display Pizzas in Queue" << endl;
    cout << "0. Exit" << endl;
    cout << "Enter your choice: ";
}

int main() {
    Stack stack; // Instance of the Stack class
    Queue queue; // Instance of the Queue class
    int choice; // Variable to store user's menu choice
    Topping topping; // Struct to hold topping information
    Pizza pizza; // Struct to hold pizza information

    do {
        displayMenu();
        cin >> choice; 
        cin.ignore(); 
        cout << endl;

        switch (choice) {
            case 1: { // Push Topping to Stack
                cout << "Enter topping name: ";
                char name[100]; // Array to store topping name
                cin.get(name, 100, '\n');
                cin.ignore();

                cout << "Enter topping amount: ";
                int amount; // Variable to store topping amount
                cin >> amount;
                cin.ignore();

                cout << "Enter topping calories: ";
                int calories; // Variable to store topping calories
                cin >> calories;
                cin.ignore();

                cout << "Enter topping color: ";
                char color[100]; // Array to store topping color
                cin.get(color, 100, '\n');
                cin.ignore();

                // Dynamically allocate memory for topping members
                topping.name = new char[strlen(name) + 1];
                strcpy(topping.name, name);
                topping.amount = amount;
                topping.calories = calories;
                topping.colorOfTopping = new char[strlen(color) + 1];
                strcpy(topping.colorOfTopping, color);

                stack.push(topping); // Push the topping to the stack
                break;
            }
            case 2: { // Pop Topping from Stack
                stack.pop(topping); // Pop a topping from the stack
                cout << "Popped topping: " << topping.name << endl;
                delete[] topping.name; // Free dynamically allocated memory
                delete[] topping.colorOfTopping;
                break;
            }
            case 3: { // Peek Top Topping in Stack
                stack.peek(topping); // Peek the top topping in the stack
                cout << "Top topping: " << topping.name << endl;
                break;
            }
            case 4: { // Display Toppings in Stack
                stack.display(); // Display all toppings in the stack
                break;
            }
            case 5: { // Enqueue Pizza to Queue
                cout << "Enter pizza name: ";
                char name[100]; // Array to store pizza name
                cin.get(name, 100, '\n');
                cin.ignore();

                cout << "Enter pizza sauce: ";
                char sauce[100]; // Array to store pizza sauce
                cin.get(sauce, 100, '\n');
                cin.ignore();

                cout << "Enter number of toppings: ";
                int numToppings; // Variable to store the number of toppings
                cin >> numToppings;
                cin.ignore();

                cout << "Enter pizza size: ";
                char size[100]; // Array to store pizza size
                cin.get(size, 100, '\n');
                cin.ignore();

                // Dynamically allocate memory for pizza members
                pizza.name = new char[strlen(name) + 1];
                strcpy(pizza.name, name);
                pizza.sauce = new char[strlen(sauce) + 1];
                strcpy(pizza.sauce, sauce);
                pizza.numToppings = numToppings;
                pizza.size = new char[strlen(size) + 1];
                strcpy(pizza.size, size);

                queue.enqueue(pizza); // Enqueue the pizza to the queue
                break;
            }
            case 6: { // Dequeue Pizza from Queue
                pizza = queue.dequeue(); // Dequeue a pizza from the queue
                cout << "Dequeued pizza: " << pizza.name << endl;
                delete[] pizza.name; // Free dynamically allocated memory
                delete[] pizza.sauce;
                delete[] pizza.size;
                break;
            }
            case 7: { // Peek Next Pizza in Queue
                pizza = queue.peek(); // Peek the next pizza in the queue
                cout << "Next pizza: " << pizza.name << endl;
                break;
            }
            case 8: { // Display Pizzas in Queue
                queue.display(); // Display all pizzas in the queue
                break;
            }
            case 0: { // Exit
                cout << "Exiting the program." << endl;
                break;
            }
            default: {
                cout << "Invalid choice. Please try again." << endl;
                break;
            }
        }

        cout << endl;
    } while (choice != 0); // Continue the loop until user chooses to exit

    return 0;
}