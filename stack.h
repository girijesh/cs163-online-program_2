// Girijesh Thodupunuri
// CS 163
// Program 2

// This file contains the declaration of the Stack class.
// The Stack class is used to manage toppings in the Pizza Management System.
// It uses a linear linked list of arrays to store and retrieve Topping objects.

#include "pizza.h"

// Node struct: Represents a node in the linear linked list of arrays
struct Node {
    Topping toppings[5]; // Array of size 5 to store Topping objects
    int topIndex;        // Index of the top Topping object in the array
    Node* next;          // Pointer to the next node in the list
};

class Stack {
public:
    // Constructor: Initializes the stack
    Stack();

    // Destructor: Deallocates the memory used by the stack
    ~Stack();

    // Push: Adds a new Topping object to the top of the stack
    void push(const Topping& topping);

    // Pop: Removes and returns the Topping object from the top of the stack
    void pop(Topping& topping);

    // Peek: Returns the Topping object at the top of the stack without removing it
    void peek(Topping& topping);

    // Display: Prints the details of all Topping objects in the stack
    void display();

private:
    Node* head; // Pointer to the head node of the linked list
};